/**
 * @class {ExtendedWarrantyRequestRuleSimpleSerialCodeWebView}
 */
export default class ExtendedWarrantyRequestRuleSimpleSerialCodeWebView {

    _serialNumber:string;


    /**
     * @param serialNumber
     */
    constructor(
        serialNumber:string
    ) {
        this._serialNumber = serialNumber;
    }

    get serialNumber():string {
        return this._serialNumber;
    }

    toJSON() {
        return {
            serialNumber:this._serialNumber
        }
    }
}
