import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ProductSaleRegistrationRuleEngineWebDto from './productSaleRegistrationRuleEngineWebDto';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';

@inject(PartnerSaleRegistrationDraftServiceSdkConfig, HttpClient)
class IsEligibleForSpiffFeature {

    _config:PartnerSaleRegistrationDraftServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param {ProductSaleRegistrationRuleEngineWebDto} request
     * @param {string} accessToken
     * @returns {Promise.<boolean>}
     */
    execute(request:ProductSaleRegistrationRuleEngineWebDto,
            accessToken:string):Promise<boolean> {

        return this._httpClient
            .createRequest('partner-sale-registration/isspiffeligible')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default IsEligibleForSpiffFeature;