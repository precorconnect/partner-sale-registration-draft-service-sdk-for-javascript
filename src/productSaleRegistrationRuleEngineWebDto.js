import PartnerSaleSimpleLineItemWebDto from './partnerSaleSimpleLineItemWebDto';
import PartnerSaleCompositeLineItemWebDto from './partnerSaleCompositeLineItemWebDto';

/**
 * @class {ProductSaleRegistrationRuleEngineWebDto}
 */
export default class ProductSaleRegistrationRuleEngineWebDto{

    _sellDate:string;

    _installDate:string;

    _facilityBrand:string;

    _simpleLineItems:PartnerSaleSimpleLineItemWebDto[] ;

    _compositeLineItems:PartnerSaleCompositeLineItemWebDto[];

    constructor(
        sellDate:string,
        installDate:string,
        facilityBrand:string,
        simpleLineItems:PartnerSaleSimpleLineItemWebDto[],
        compositeLineItems:PartnerSaleCompositeLineItemWebDto[]
    ){

        if(!sellDate){
            throw new TypeError('sellDate required');
        }
        this._sellDate = sellDate;

        this._installDate = installDate;

        this._facilityBrand = facilityBrand;

        this._simpleLineItems = simpleLineItems;

        this._compositeLineItems = compositeLineItems;

    }

    /**
     * getter methods
     */
    get sellDate():string{
        return this._sellDate;
    }

    get installDate():string{
        return this._installDate;
    }

    get facilityBrand():string{
        return this._facilityBrand;
    }

    get simpleLineItems():PartnerSaleSimpleLineItemWebDto[]{
        return this._simpleLineItems;
    }

    get compositeLineItems():PartnerSaleCompositeLineItemWebDto[]{
        return this._compositeLineItems;
    }

    toJSON() {
        return {
            sellDate: this._sellDate,
            installDate: this._installDate,
            facilityBrand: this._facilityBrand,
            simpleLineItems: this._simpleLineItems,
            compositeLineItems: this._compositeLineItems
        }
    }

}

