import ExtendedWarrantyRequestRuleSimpleSerialCodeWebView from './extendedWarrantyRequestRuleSimpleSerialCodeWebView';
/**
 * @class {ExtendedWarrantyRequestRuleCompositeSerialCodeWebView}
 */
export default class ExtendedWarrantyRequestRuleCompositeSerialCodeWebView {

    _components:ExtendedWarrantyRequestRuleSimpleSerialCodeWebView[];

    constructor(
        components:ExtendedWarrantyRequestRuleSimpleSerialCodeWebView[]
    ){
        this._components = components;
    }


    get components():ExtendedWarrantyRequestRuleSimpleSerialCodeWebView[] {
        return this._components;
    }

    toJSON() {
        return {
            components : this._components
        }
    }
}
